import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        //arraylist de librerias
        int totalDays;
        String path = "C:/Users/abrug/IdeaProjects/hashcode2020/src/data/f_libraries_of_the_world.txt";

        Pair<Integer, ArrayList<Library>> init = Reader.read(path);
        totalDays = init.getKey();
        ArrayList<Library> libraries = init.getValue();
        Collections.sort(libraries);
        int[] booksScanned = new int[999999];


        int librariesScanned = 0;

        for (int i = 0; i < totalDays; ++i){

            for (int j = 0; j < libraries.size(); j++){
                Library library = libraries.get(j);

                if (library.daysToSignUp > 0){
                    if (--library.daysToSignUp == 0) ++librariesScanned;
                    continue;
                }

                for (int k = 0; k < library.booksPerDay; ++k){
                    Book auxBook;
                    if (library.books.size() == 0){
                        break;
                    } else if (k >= library.books.size()){
                        auxBook = library.books.get(0);
                    } else{
                        auxBook = library.books.get(k);
                    }

                    if (auxBook.id < booksScanned.length){
                        if (booksScanned[auxBook.id] == 1){
                            if(k >= library.books.size()){
                                library.books.remove(0);
                            } else {
                                library.books.remove(k);
                            }
                            continue;
                        }
                    }
                    library.booksScanned.add(auxBook);
                    booksScanned[auxBook.id] = 1;

                }
            }
        }

        Writer.write(librariesScanned, libraries);

    }
}
