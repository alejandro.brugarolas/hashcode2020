import javafx.util.Pair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Reader {
    public static Pair<Integer, ArrayList<Library>> read(String path){
        Pair<Integer, ArrayList<Library>> response;
        ArrayList<Library>  data = new ArrayList<>();
        List<String> lines = new ArrayList<>();
        ArrayList<Book> books = new ArrayList<>();
        try {
             lines = Files.readAllLines(Path.of(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        int booksNumber;
        int libraries;
        int days;
        String[] firstInfo = lines.get(0).split(" ");

        //lee primera línea
        booksNumber = Integer.valueOf(firstInfo[0]);
        libraries = Integer.valueOf(firstInfo[1]);
        days = Integer.valueOf(firstInfo[2]);

        //System.out.println("Primera línea\nLibros: " + booksNumber + "\nBibliotecas: " + libraries + "\n" + "Días: " + days);

        //lee libros
        String[] booksScore = lines.get(1).split(" ");
        for (int i = 0; i < booksNumber; i++){
            books.add(new Book(Integer.valueOf(booksScore[i]), i));
        }

        int numberOfBooksPerLibrary = 0;
        int lineCounter = 1;
        for (int i = 0; i < libraries; ++i){

            Library library = new Library();
            library.id = i;
            String[] libraryData = lines.get(++lineCounter).split(" ");
            numberOfBooksPerLibrary = Integer.valueOf(libraryData[0]);
            library.daysToSignUp = Integer.valueOf(libraryData[1]);
            library.booksPerDay = Integer.valueOf(libraryData[2]);

            String booksIds[] = lines.get(++lineCounter).split(" ");
            for (int j = 0; j < numberOfBooksPerLibrary; ++j){
                Book auxBook = books.get(Integer.valueOf(booksIds[j]));
                library.value += auxBook.score;
                library.books.add(auxBook);
            }
            //library.value += (library.daysToSignUp + Math.ceil((double)library.books.size() / (double)library.booksPerDay));
            Collections.sort(library.books);
            data.add(library);
            //System.out.println(library);
        }

        response = new Pair<>(days, data);
        return response;

    }
}
