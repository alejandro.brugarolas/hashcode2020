public class Book implements Comparable {
    public int score, id;

    public Book(int score, int id) {
        this.score = score;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Book{" +
                "score=" + score +
                ", id=" + id +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if (this.score ==( (Book)o).score) return 0;
        return this.score <( (Book)o).score ? 1 : -1;
    }
}
