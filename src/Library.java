import java.util.ArrayList;

public class Library implements Comparable{
    public int id, booksPerDay, daysToSignUp, value;
    public ArrayList<Book> books, booksScanned;

    public Library(int id, int booksPerDay, int daysToSignUp) {
        this.id = id;
        this.booksPerDay = booksPerDay;
        this.daysToSignUp = daysToSignUp;
        this.books = new ArrayList<>();
        this.booksScanned = new ArrayList<>();
        this.value = 0;
    }

    public Library() {
        this.books = new ArrayList<>();
        this.booksScanned = new ArrayList<>();
        this.value = 0;
    }

    @Override
    public String toString() {
        return "Library{" +
                "id=" + id +
                ", booksPerDay=" + booksPerDay +
                ", daysToSignUp=" + daysToSignUp +
                ", value=" + value +
                ", books=" + books +
                '}';
    }


    @Override
    public int compareTo(Object o) {
        if (this.value ==( (Library)o).value ) return 0;
        return this.value <( (Library)o).value ? 1 : -1;
    }
}
