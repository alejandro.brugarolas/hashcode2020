import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Writer {
    public static void write(int librariesScanned, ArrayList<Library> libraries){
        try {
            String path = "C:/Users/abrug/IdeaProjects/hashcode2020/src/data/out.txt";
            File myObj = new File(path);
            FileWriter writer = new FileWriter(path);
            StringBuilder sb = new StringBuilder();
            //sb.append(librariesScanned + "\n");
            int aux = librariesScanned;
            int k;
            for (int i = 0; i < librariesScanned; ++i){
                Library auxLibrary = libraries.get(i);
                if (auxLibrary.booksScanned.size() == 0){
                    aux--;
                    continue;
                }

                sb.append(auxLibrary.id + " " + auxLibrary.booksScanned.size() + "\n");
                for (k = 0; k < auxLibrary.booksScanned.size(); ++k){
                    if (k != 0) sb.append(" ");
                    sb.append(auxLibrary.booksScanned.get(k).id);
                }
                if (k != 0){
                    sb.append("\n");
                }


            }
            writer.write(aux + "\n" + sb.toString());
            writer.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
}
